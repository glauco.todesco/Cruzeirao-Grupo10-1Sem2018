package dados;

import java.util.ArrayList;

import sistema.modelos.Campeonato;
import sistema.modelos.Cartao;
import sistema.modelos.Categoria;
import sistema.modelos.Equipe;
import sistema.modelos.Fase;
import sistema.modelos.Gol;
import sistema.modelos.Grupo;
import sistema.modelos.Inscricao;
import sistema.modelos.Inscrito;
import sistema.modelos.Juiz;
import sistema.modelos.Local;
import sistema.modelos.Partida;
import sistema.modelos.Rodada;
import sistema.modelos.Usuario;

public class Dados {
	public static ArrayList<Campeonato> campeonatos = new ArrayList<Campeonato>();
	public static ArrayList<Cartao> cartaos = new ArrayList<Cartao>();
	public static ArrayList<Categoria> categorias = new ArrayList<Categoria>();
	public static ArrayList<Equipe> equipes = new ArrayList<Equipe>();
	public static ArrayList<Fase> fases = new ArrayList<Fase>();
	public static ArrayList<Gol> gols = new ArrayList<Gol>();
	public static ArrayList<Grupo> grupos = new ArrayList<Grupo>();
	public static ArrayList<Inscricao> inscricoes = new ArrayList<Inscricao>();
	public static ArrayList<Inscrito> inscritos = new ArrayList<Inscrito>();
	public static ArrayList<Juiz> juizes = new ArrayList<Juiz>();
	public static ArrayList<Local> locais = new ArrayList<Local>();
	public static ArrayList<Partida> partidas = new ArrayList<Partida>();
	public static ArrayList<Rodada> rodadas = new ArrayList<Rodada>();
	public static ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
}