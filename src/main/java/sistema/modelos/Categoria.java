package sistema.modelos;

import java.util.List;

import Enums.Sexo;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Categoria")
public class Categoria implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idCategoria;
	@Column(name="Nome", nullable=false)
	private String nome;
	@Column(name="Nascidos_A_Partir_De")
	private int nascidosApartirDe;
	@OneToMany(mappedBy="categoria")
	private List <Inscricao> inscricoes;
	@ManyToOne
	private Campeonato campeonato;
	@OneToMany(mappedBy="categoria")
	private List <Fase> fases;
	@Column(name="Min_Jogadores", nullable=false)
	private int minJogadores;
	@Column(name="Max_Jogadores", nullable=false)
	private int maxJogadores;
	@Column(name="Sexo", nullable=false)
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	
	// Getters e Setters
	public int getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getNascidosApartirDe() {
		return nascidosApartirDe;
	}
	public void setNascidosApartirDe(int nascidosApartirDe) {
		this.nascidosApartirDe = nascidosApartirDe;
	}
	public List<Inscricao> getInscricoes() {
		return inscricoes;
	}
	public void setInscricoes(List<Inscricao> inscricoes) {
		this.inscricoes = inscricoes;
	}
	public Campeonato getCampeonato() {
		return campeonato;
	}
	public void setCampeonato(Campeonato campeonato) {
		this.campeonato = campeonato;
	}
	public List<Fase> getFases() {
		return fases;
	}
	public void setFases(List<Fase> fases) {
		this.fases = fases;
	}
	public int getMinJogadores() {
		return minJogadores;
	}
	public void setMinJogadores(int minJogadores) {
		this.minJogadores = minJogadores;
	}
	public int getMaxJogadores() {
		return maxJogadores;
	}
	public void setMaxJogadores(int maxJogadores) {
		this.maxJogadores = maxJogadores;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	
	//Add's
	public void addInscricao(Inscricao inscricao)
	{
		inscricoes.add(inscricao);
	}
	public void addFase(Fase fase)
	{
		fases.add(fase);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + ((campeonato == null) ? 0 : campeonato.hashCode());
		//result = prime * result + ((fases == null) ? 0 : fases.hashCode());
		result = prime * result + idCategoria;
		//result = prime * result + ((inscricoes == null) ? 0 : inscricoes.hashCode());
		result = prime * result + maxJogadores;
		result = prime * result + minJogadores;
		result = prime * result + nascidosApartirDe;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((sexo == null) ? 0 : sexo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categoria other = (Categoria) obj;
		if (campeonato == null) {
			if (other.campeonato != null)
				return false;
		}
		//else if (!campeonato.equals(other.campeonato))
		//	return false;
		if (fases == null) {
			if (other.fases != null)
				return false;
		}
		//else if (!fases.equals(other.fases))
		//	return false;
		if (idCategoria != other.idCategoria)
			return false;
		if (inscricoes == null) {
			if (other.inscricoes != null)
				return false;
		}
		//else if (!inscricoes.equals(other.inscricoes))
		//	return false;
		if (maxJogadores != other.maxJogadores)
			return false;
		if (minJogadores != other.minJogadores)
			return false;
		if (nascidosApartirDe != other.nascidosApartirDe)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sexo != other.sexo)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Categoria [idCategoria=" + idCategoria + ", nome=" + nome + ", nascidosApartirDe=" + nascidosApartirDe
				+ ", inscricoes=" + inscricoes + ", campeonato=" + campeonato + ", fases=" + fases + ", minJogadores="
				+ minJogadores + ", maxJogadores=" + maxJogadores + ", sexo=" + sexo + "]";
	}
}
