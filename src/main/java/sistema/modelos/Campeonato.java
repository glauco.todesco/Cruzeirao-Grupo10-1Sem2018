package sistema.modelos;

import java.util.List;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="Campeonato")
public class Campeonato implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idCampeonato;
	@Column(name="Nome", nullable=false)
	private String nome;
	@OneToMany
	private List <Local> locais;
	@OneToMany
	private List <Juiz> juizes;
	@OneToMany(mappedBy="campeonato")
	private List <Categoria> categorias;
	@Column(name="Data_Ini_Insc", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataInicioInscricao;
	@Column(name="Data_Fim_Insc", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataFimInscricao;
	@Column(name="Data_Ini_Camp", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataInicioCampeonato;
	@Column(name="Data_Fim_Camp", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataFimCampeonato;
	@Column(name="ValorTaxa")
	private double valorTaxa;
	
	// Getters e Setters
	public int getIdCampeonato() {
		return idCampeonato;
	}
	public void setIdCampeonato(int idCampeonato) {
		this.idCampeonato = idCampeonato;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Local> getLocais() {
		return locais;
	}
	public void setLocais(List<Local> locais) {
		this.locais = locais;
	}
	public List<Juiz> getJuizes() {
		return juizes;
	}
	public void setJuizes(List<Juiz> juizes) {
		this.juizes = juizes;
	}
	public List<Categoria> getCategorias() {
		return categorias;
	}
	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}
	public Date getDataInicioInscricao() {
		return dataInicioInscricao;
	}
	public void setDataInicioInscricao(Date dataInicioInscricao) {
		this.dataInicioInscricao = dataInicioInscricao;
	}
	public Date getDataFimInscricao() {
		return dataFimInscricao;
	}
	public void setDataFimInscricao(Date dataFimInscricao) {
		this.dataFimInscricao = dataFimInscricao;
	}
	public Date getDataInicioCampeonato() {
		return dataInicioCampeonato;
	}
	public void setDataInicioCampeonato(Date dataInicioCampeonato) {
		this.dataInicioCampeonato = dataInicioCampeonato;
	}
	public Date getDataFimCampeonato() {
		return dataFimCampeonato;
	}
	public void setDataFimCampeonato(Date dataFimCampeonato) {
		this.dataFimCampeonato = dataFimCampeonato;
	}
	public double getValorTaxa() {
		return valorTaxa;
	}
	public void setValorTaxa(double valorTaxa) {
		this.valorTaxa = valorTaxa;
	}
	
	//Add's
	public void addLocal(Local local)
	{
		locais.add(local);
	}
	public void addJuiz(Juiz juiz)
	{
		juizes.add(juiz);
	}
	public void addCategoria(Categoria categoria)
	{
		categorias.add(categoria);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + ((categorias == null) ? 0 : categorias.hashCode());
		result = prime * result + ((dataFimCampeonato == null) ? 0 : dataFimCampeonato.hashCode());
		result = prime * result + ((dataFimInscricao == null) ? 0 : dataFimInscricao.hashCode());
		result = prime * result + ((dataInicioCampeonato == null) ? 0 : dataInicioCampeonato.hashCode());
		result = prime * result + ((dataInicioInscricao == null) ? 0 : dataInicioInscricao.hashCode());
		result = prime * result + idCampeonato;
		result = prime * result + ((juizes == null) ? 0 : juizes.hashCode());
		result = prime * result + ((locais == null) ? 0 : locais.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		long temp;
		temp = Double.doubleToLongBits(valorTaxa);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campeonato other = (Campeonato) obj;
		if (categorias == null) {
			if (other.categorias != null)
				return false;
		}
		//else if (!categorias.equals(other.categorias))
		//	return false;
		if (dataFimCampeonato == null) {
			if (other.dataFimCampeonato != null)
				return false;
		}
		else if (!dataFimCampeonato.equals(other.dataFimCampeonato))
			return false;
		if (dataFimInscricao == null) {
			if (other.dataFimInscricao != null)
				return false;
		}
		else if (!dataFimInscricao.equals(other.dataFimInscricao))
			return false;
		if (dataInicioCampeonato == null) {
			if (other.dataInicioCampeonato != null)
				return false;
		}
		else if (!dataInicioCampeonato.equals(other.dataInicioCampeonato))
			return false;
		if (dataInicioInscricao == null) {
			if (other.dataInicioInscricao != null)
				return false;
		}
		else if (!dataInicioInscricao.equals(other.dataInicioInscricao))
			return false;
		if (idCampeonato != other.idCampeonato)
			return false;
		if (juizes == null) {
			if (other.juizes != null)
				return false;
		}
		else if (!juizes.equals(other.juizes))
			return false;
		if (locais == null) {
			if (other.locais != null)
				return false;
		}
		else if (!locais.equals(other.locais))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		}
		else if (!nome.equals(other.nome))
			return false;
		if (Double.doubleToLongBits(valorTaxa) != Double.doubleToLongBits(other.valorTaxa))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Campeonato [idCampeonato=" + idCampeonato + ", nome=" + nome + ", locais=" + locais + ", juizes="
				+ juizes + ", categorias=" + categorias + ", dataInicioInscricao=" + dataInicioInscricao
				+ ", dataFimInscricao=" + dataFimInscricao + ", dataInicioCampeonato=" + dataInicioCampeonato
				+ ", dataFimCampeonato=" + dataFimCampeonato + ", valorTaxa=" + valorTaxa + "]";
	}
}