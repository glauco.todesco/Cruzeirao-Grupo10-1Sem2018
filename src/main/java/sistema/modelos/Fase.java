package sistema.modelos;

import java.util.List;

import Enums.FormatoFase;

import java.util.Date;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Fase")
public class Fase implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int numero;
	@Column(name="Data_Inicio", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataInicio;
	@Column(name="Data_Fim", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataFim;
	@Column(name="Formato", nullable=false)
	@Enumerated(EnumType.STRING)
	private FormatoFase formato;
	@ManyToOne
	private Categoria categoria;
	@OneToMany(mappedBy="fase")
	private List <Grupo> grupos;
	
	// Getters e Setters
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public List<Grupo> getGrupos() {
		return grupos;
	}
	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}
	public FormatoFase getFormato() {
		return formato;
	}
	public void setFormato(FormatoFase formato) {
		this.formato = formato;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	//Add's
	public void addGrupo(Grupo grupo)
	{
		grupos.add(grupo);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((dataFim == null) ? 0 : dataFim.hashCode());
		result = prime * result + ((dataInicio == null) ? 0 : dataInicio.hashCode());
		result = prime * result + ((formato == null) ? 0 : formato.hashCode());
		result = prime * result + ((grupos == null) ? 0 : grupos.hashCode());
		result = prime * result + numero;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fase other = (Fase) obj;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		}
		//else if (!categoria.equals(other.categoria))
		//	return false;
		if (dataFim == null) {
			if (other.dataFim != null)
				return false;
		}
		else if (!dataFim.equals(other.dataFim))
			return false;
		if (dataInicio == null) {
			if (other.dataInicio != null)
				return false;
		}
		else if (!dataInicio.equals(other.dataInicio))
			return false;
		if (formato != other.formato)
			return false;
		if (grupos == null) {
			if (other.grupos != null)
				return false;
		}
		else if (!grupos.equals(other.grupos))
			return false;
		if (numero != other.numero)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Fase [numero=" + numero + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", formato=" + formato
				+ ", categoria=" + categoria + ", grupos=" + grupos + "]";
	}
}
