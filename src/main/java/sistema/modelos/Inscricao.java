package sistema.modelos;

import java.util.List;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Inscricao")
public class Inscricao implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long numero;
	@Column(name="Pagamento", nullable=false)
	private boolean pagamento;
	@Column(name="Validada", nullable=false)
	private boolean validada;
	@OneToMany(mappedBy="inscricao")
	private List <Inscrito> inscritos;
	@ManyToOne
	private Categoria categoria;
	@OneToMany
	private List <Partida> partidas;
	@Column(name="Equipe", nullable=false)
	private String equipe;
	
	// Getters e Setters
	public boolean isPagamento() {
		return pagamento;
	}
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public void setPagamento(boolean pagamento) {
		this.pagamento = pagamento;
	}
	public boolean isValidada() {
		return validada;
	}
	public void setValidada(boolean validada) {
		this.validada = validada;
	}
	public List<Inscrito> getInscritos() {
		return inscritos;
	}
	public void setInscritos(List<Inscrito> inscritos) {
		this.inscritos = inscritos;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public List<Partida> getPartidas() {
		return partidas;
	}
	public void setPartidas(List<Partida> partidas) {
		this.partidas = partidas;
	}
	public String getEquipe() {
		return equipe;
	}
	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}
	
	//Add's
	public void addInscrito(Inscrito inscrito)
	{
		inscritos.add(inscrito);
	}
	public void addPartida(Partida partida)
	{
		partidas.add(partida);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((equipe == null) ? 0 : equipe.hashCode());
		result = prime * result + ((inscritos == null) ? 0 : inscritos.hashCode());
		result = prime * result + (int) (numero ^ (numero >>> 32));
		result = prime * result + (pagamento ? 1231 : 1237);
		result = prime * result + ((partidas == null) ? 0 : partidas.hashCode());
		result = prime * result + (validada ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inscricao other = (Inscricao) obj;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		}
		//else if (!categoria.equals(other.categoria))
		//	return false;
		if (equipe == null) {
			if (other.equipe != null)
				return false;
		}
		else if (!equipe.equals(other.equipe))
			return false;
		if (inscritos == null) {
			if (other.inscritos != null)
				return false;
		}
		else if (!inscritos.equals(other.inscritos))
			return false;
		if (numero != other.numero)
			return false;
		if (pagamento != other.pagamento)
			return false;
		if (partidas == null) {
			if (other.partidas != null)
				return false;
		}
		else if (!partidas.equals(other.partidas))
			return false;
		if (validada != other.validada)
			return false;
		return true;
	}
	//@Override
	//public String toString() {
	//	return "Inscricao [numero=" + numero + ", pagamento=" + pagamento + ", validada=" + validada + ", inscritos="
	//			+ inscritos + ", categoria=" + categoria + ", partidas=" + partidas + ", equipe=" + equipe + "]";
	//}
}
