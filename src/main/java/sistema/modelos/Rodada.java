package sistema.modelos;

import java.util.List;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@Table(name="Rodada")
public class Rodada implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int numero;
	@ManyToOne
	private Grupo grupo;
	@OneToMany
	private List <Partida> partidas;
	
	// Getters e Setters
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public List<Partida> getPartidas() {
		return partidas;
	}
	public void setPartidas(List<Partida> partidas) {
		this.partidas = partidas;
	}
	
	//Add's
	public void addPartida(Partida partida)
	{
		partidas.add(partida);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grupo == null) ? 0 : grupo.hashCode());
		result = prime * result + numero;
		result = prime * result + ((partidas == null) ? 0 : partidas.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rodada other = (Rodada) obj;
		if (grupo == null) {
			if (other.grupo != null)
				return false;
		}
		else if (!grupo.equals(other.grupo))
			return false;
		if (numero != other.numero)
			return false;
		if (partidas == null) {
			if (other.partidas != null)
				return false;
		}
		else if (!partidas.equals(other.partidas))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Rodada [numero=" + numero + ", grupo=" + grupo + ", partidas=" + partidas + "]";
	}
}
