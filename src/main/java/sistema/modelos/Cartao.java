package sistema.modelos;

import Enums.TipoCartao;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Cartao")
public class Cartao implements Serializable 
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idCartao;
	@ManyToOne
	private Inscrito inscrito;
	@Column(name="Tipo_Cartao", nullable=false)
	@Enumerated(EnumType.STRING) 
	private TipoCartao tipo;
	@Column(name="Tempo", nullable=false)
	private int tempo;
	
	// Getters e Setters
	public Inscrito getInscrito() {
		return inscrito;
	}
	public int getIdCartao() {
		return idCartao;
	}
	public void setIdCartao(int idCartao) {
		this.idCartao = idCartao;
	}
	public void setInscrito(Inscrito inscrito) {
		this.inscrito = inscrito;
	}
	public TipoCartao getTipo() {
		return tipo;
	}
	public void setTipo(TipoCartao tipo) {
		this.tipo = tipo;
	}
	public int getTempo() {
		return tempo;
	}
	public void setTempo(int tempo) {
		this.tempo = tempo;
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCartao;
		result = prime * result + ((inscrito == null) ? 0 : inscrito.hashCode());
		result = prime * result + tempo;
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cartao other = (Cartao) obj;
		if (idCartao != other.idCartao)
			return false;
		if (inscrito == null) {
			if (other.inscrito != null)
				return false;
		}
		else if (!inscrito.equals(other.inscrito))
			return false;
		if (tempo != other.tempo)
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Cartao [idCartao=" + idCartao + ", inscrito=" + inscrito + ", tipo=" + tipo + ", tempo=" + tempo + "]";
	}
}
