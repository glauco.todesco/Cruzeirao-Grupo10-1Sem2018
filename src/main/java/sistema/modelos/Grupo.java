package sistema.modelos;

import java.util.List;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Grupo")
public class Grupo implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int numero;
	@Column(name="Nome", nullable=false)
	private String nome;
	@ManyToOne
	private Fase fase;
	@OneToMany(mappedBy="grupo")
	private List <Rodada> rodadas;
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Fase getFase() {
		return fase;
	}
	public void setFase(Fase fase) {
		this.fase = fase;
	}
	public List<Rodada> getRodadas() {
		return rodadas;
	}
	public void setRodadas(List<Rodada> rodadas) {
		this.rodadas = rodadas;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	//Add's
	public void addRodada(Rodada rodada)
	{
		rodadas.add(rodada);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fase == null) ? 0 : fase.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + numero;
		result = prime * result + ((rodadas == null) ? 0 : rodadas.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupo other = (Grupo) obj;
		if (fase == null) {
			if (other.fase != null)
				return false;
		}
		else if (!fase.equals(other.fase))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		}
		else if (!nome.equals(other.nome))
			return false;
		if (numero != other.numero)
			return false;
		if (rodadas == null) {
			if (other.rodadas != null)
				return false;
		}
		else if (!rodadas.equals(other.rodadas))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Grupo [numero=" + numero + ", nome=" + nome + ", fase=" + fase + ", rodadas=" + rodadas + "]";
	}
}
