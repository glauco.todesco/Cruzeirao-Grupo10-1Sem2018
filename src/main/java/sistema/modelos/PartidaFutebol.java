package sistema.modelos;

import java.util.List;

import javax.persistence.OneToMany;

public class PartidaFutebol extends Partida
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@OneToMany
	private List <Gol> golsMandantes;
	@OneToMany
	private List <Gol> golsVisitantes;
	@OneToMany
	private List <Gol> golsPenaltyMandantes;
	@OneToMany
	private List <Gol> golsPenaltyVisitantes;
	@OneToMany
	private List <Cartao> cartoesMandantes;
	@OneToMany
	private List <Cartao> cartoesVisitantes;
	
	// Getters e Setters
	public List<Gol> getGolsMandantes() {
		return golsMandantes;
	}
	public void setGolsMandantes(List<Gol> golsMandantes) {
		this.golsMandantes = golsMandantes;
	}
	public List<Gol> getGolsVisitantes() {
		return golsVisitantes;
	}
	public void setGolsVisitantes(List<Gol> golsVisitantes) {
		this.golsVisitantes = golsVisitantes;
	}
	public List<Gol> getGolsPenaltyMandantes() {
		return golsPenaltyMandantes;
	}
	public void setGolsPenaltyMandantes(List<Gol> golsPenaltyMandantes) {
		this.golsPenaltyMandantes = golsPenaltyMandantes;
	}
	public List<Gol> getGolsPenaltyVisitantes() {
		return golsPenaltyVisitantes;
	}
	public void setGolsPenaltyVisitantes(List<Gol> golsPenaltyVisitantes) {
		this.golsPenaltyVisitantes = golsPenaltyVisitantes;
	}
	public List<Cartao> getCartoesMandantes() {
		return cartoesMandantes;
	}
	public void setCartoesMandantes(List<Cartao> cartoesMandantes) {
		this.cartoesMandantes = cartoesMandantes;
	}
	public List<Cartao> getCartoesVisitantes() {
		return cartoesVisitantes;
	}
	public void setCartoesVisitantes(List<Cartao> cartoesVisitantes) {
		this.cartoesVisitantes = cartoesVisitantes;
	}
	
	//Add's
	public void addGol(Gol gol)
	{
		Inscricao equipeGol=gol.getInscrito().getInscricao();
		
		if(equipeGol.equals(this.getEquipeMandante()))
		{
			if(gol.isPenalty()==true)
				golsPenaltyMandantes.add(gol);
			else
				golsMandantes.add(gol);
		}
		else if(equipeGol.equals(this.getEquipeVisitante())) 
		{
			if(gol.isPenalty()==true)
				golsPenaltyVisitantes.add(gol);
			else
				golsVisitantes.add(gol);
		}
	}
	public void addCartao(Cartao cartao)
	{
		Inscricao equipeCartao=cartao.getInscrito().getInscricao();
		
		if(equipeCartao.equals(this.getEquipeMandante()))
			cartoesMandantes.add(cartao);
		else if(equipeCartao.equals(this.getEquipeVisitante()))
			cartoesVisitantes.add(cartao);
	}
}
