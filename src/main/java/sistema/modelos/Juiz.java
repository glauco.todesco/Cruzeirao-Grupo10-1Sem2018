package sistema.modelos;

import Enums.Tipo;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Juiz")
public class Juiz implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idJuiz;
	@Column(name="Tipo", nullable=false)
	@Enumerated(EnumType.STRING) 
	private Tipo tipo=Tipo.JUIZ;
	@ManyToOne
	private Usuario usuario;
	
	// Getters e Setters
	public int getIdJuiz() {
		return idJuiz;
	}
	public void setIdJuiz(int idJuiz) {
		this.idJuiz = idJuiz;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
			this.usuario = usuario;
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idJuiz;
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Juiz other = (Juiz) obj;
		if (idJuiz != other.idJuiz)
			return false;
		if (tipo != other.tipo)
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		}
		else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Juiz [idJuiz=" + idJuiz + ", tipo=" + tipo + ", usuario=" + usuario + "]";
	}
}
