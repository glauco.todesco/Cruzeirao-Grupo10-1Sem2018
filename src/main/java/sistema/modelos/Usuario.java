package sistema.modelos;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import java.io.Serializable;

import Enums.Sexo;
import Enums.Tipo;


@Entity
@Table(name="Usuario")
public class Usuario implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idUsuario;
	@Column(name="Email", nullable=false)
	private String email;
	@Column(name="Nome", nullable=false)
	private String nome;
	@Column(name="Data_Nascimento", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	@ManyToMany(mappedBy="diretores")
	private List <Equipe> equipes;
	@OneToMany(mappedBy="usuario")
	private List <Inscrito> inscricoes;
	@OneToMany
	private List <Campeonato> campeonatos;
	@Column(name="Tipo", nullable=false)
	@Enumerated(EnumType.STRING)
	private Tipo tipo;
	@Column(name="Tel_Fixo", nullable=true)
	private String telFixo;
	@Column(name="Tel_Movel", nullable=false)
	private String telMovel;
	@Column(name="Endereco", nullable=false)
	private String endereco;
	@Column(name="RG", nullable=false)
	private String rg;
	@Column(name="CPF", nullable=false)
	private String cpf;
	@Column(name="CREF", nullable=true)
	private String cref;
	@Column(name="Sexo", nullable=false)
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	@Column(name="Foto", nullable=true)
	private String foto;
	
	// Getters e Setters
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Equipe> getEquipes() {
		return equipes;
	}
	public void setEquipes(List<Equipe> equipes) {
		this.equipes = equipes;
	}
	public List<Inscrito> getInscricoes() {
		return inscricoes;
	}
	public void setInscricoes(List<Inscrito> inscricoes) {
		this.inscricoes = inscricoes;
	}
	public List<Campeonato> getCampeonatos() {
		return campeonatos;
	}
	public void setCampeonatos(List<Campeonato> campeonatos) {
		this.campeonatos = campeonatos;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	public String getTelFixo() {
		return telFixo;
	}
	public void setTelFixo(String telFixo) {
		this.telFixo = telFixo;
	}
	public String getTelMovel() {
		return telMovel;
	}
	public void setTelMovel(String telMovel) {
		this.telMovel = telMovel;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCref() {
		return cref;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setCref(String cref) {
		this.cref = cref;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	//Add's
	public void addEquipe(Equipe equipe)
	{
		equipes.add(equipe);
	}
	public void addInscricao(Inscrito inscrito)
	{
		inscricoes.add(inscrito);
	}
	public void addCampeonato(Campeonato campeonato)
	{
		campeonatos.add(campeonato);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((campeonatos == null) ? 0 : campeonatos.hashCode());
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((cref == null) ? 0 : cref.hashCode());
		result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((equipes == null) ? 0 : equipes.hashCode());
		result = prime * result + ((foto == null) ? 0 : foto.hashCode());
		result = prime * result + idUsuario;
		result = prime * result + ((inscricoes == null) ? 0 : inscricoes.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((rg == null) ? 0 : rg.hashCode());
		result = prime * result + ((sexo == null) ? 0 : sexo.hashCode());
		result = prime * result + ((telFixo == null) ? 0 : telFixo.hashCode());
		result = prime * result + ((telMovel == null) ? 0 : telMovel.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (campeonatos == null) {
			if (other.campeonatos != null)
				return false;
		}
		else if (!campeonatos.equals(other.campeonatos))
			return false;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		}
		else if (!cpf.equals(other.cpf))
			return false;
		if (cref == null) {
			if (other.cref != null)
				return false;
		}
		else if (!cref.equals(other.cref))
			return false;
		if (dataNascimento == null) {
			if (other.dataNascimento != null)
				return false;
		}
		else if (!dataNascimento.equals(other.dataNascimento))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		}
		else if (!email.equals(other.email))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		}
		else if (!endereco.equals(other.endereco))
			return false;
		if (equipes == null) {
			if (other.equipes != null)
				return false;
		}
		else if (!equipes.equals(other.equipes))
			return false;
		if (foto == null) {
			if (other.foto != null)
				return false;
		}
		else if (!foto.equals(other.foto))
			return false;
		if (idUsuario != other.idUsuario)
			return false;
		if (inscricoes == null) {
			if (other.inscricoes != null)
				return false;
		}
		else if (!inscricoes.equals(other.inscricoes))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		}
		else if (!nome.equals(other.nome))
			return false;
		if (rg == null) {
			if (other.rg != null)
				return false;
		}
		else if (!rg.equals(other.rg))
			return false;
		if (sexo != other.sexo)
			return false;
		if (telFixo == null) {
			if (other.telFixo != null)
				return false;
		}
		else if (!telFixo.equals(other.telFixo))
			return false;
		if (telMovel == null) {
			if (other.telMovel != null)
				return false;
		}
		else if (!telMovel.equals(other.telMovel))
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", email=" + email + ", nome=" + nome + ", dataNascimento="
				+ dataNascimento + ", equipes=" + equipes + ", inscricoes=" + inscricoes + ", campeonatos="
				+ campeonatos + ", tipo=" + tipo + ", telFixo=" + telFixo + ", telMovel=" + telMovel + ", endereco="
				+ endereco + ", rg=" + rg + ", cpf=" + cpf + ", cref=" + cref + ", sexo=" + sexo + ", foto=" + foto
				+ "]";
	}
}
