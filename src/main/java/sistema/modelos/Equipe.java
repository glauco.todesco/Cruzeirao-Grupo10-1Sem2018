package sistema.modelos;

import java.util.Date;
import java.util.List;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Equipe")
public class Equipe implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	private int idEquipe;
	@Column(name="Nome", nullable=false)
	private String nome;
	@Column(name="Data_Fundacao", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date dataFundacao;
	@Column(name="Cidade")
	private String cidade;
	@ManyToMany
	private List <Usuario> diretores;
	
	// Getters e Setters
	public int getIdEquipe() {
		return idEquipe;
	}
	public void setIdEquipe(int idEquipe) {
		this.idEquipe = idEquipe;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataFundacao() {
		return dataFundacao;
	}
	public void setDataFundacao(Date dataFundacao) {
		this.dataFundacao = dataFundacao;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public List<Usuario> getDiretores() {
		return diretores;
	}
	public void setDiretores(List<Usuario> diretores) {
		this.diretores = diretores;
	}
	
	//Add's
	public void addDiretor(Usuario diretor)
	{
		diretores.add(diretor);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((dataFundacao == null) ? 0 : dataFundacao.hashCode());
		result = prime * result + ((diretores == null) ? 0 : diretores.hashCode());
		result = prime * result + idEquipe;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipe other = (Equipe) obj;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (dataFundacao == null) {
			if (other.dataFundacao != null)
				return false;
		} else if (!dataFundacao.equals(other.dataFundacao))
			return false;
		if (diretores == null) {
			if (other.diretores != null)
				return false;
		} else if (!diretores.equals(other.diretores))
			return false;
		if (idEquipe != other.idEquipe)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Equipe [idEquipe=" + idEquipe + ", nome=" + nome + ", dataFundacao=" + dataFundacao + ", cidade="
				+ cidade + ", diretores=" + diretores + "]";
	}
}
