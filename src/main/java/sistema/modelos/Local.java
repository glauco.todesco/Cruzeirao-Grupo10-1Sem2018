package sistema.modelos;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Local")
public class Local implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idLocal;
	@Column(name="Endereco", nullable=false)
	private String endereco;

	// Getters e Setters
	public int getIdLocal() {
		return idLocal;
	}
	public void setIdLocal(int idLocal) {
		this.idLocal = idLocal;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + idLocal;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Local other = (Local) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		}
		else if (!endereco.equals(other.endereco))
			return false;
		if (idLocal != other.idLocal)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Local [idLocal=" + idLocal + ", endereco=" + endereco + "]";
	}
}