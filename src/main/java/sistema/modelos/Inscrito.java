package sistema.modelos;

import Enums.Tipo;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Inscrito")
public class Inscrito implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idInscrito;
	@Column(name="Tipo", nullable=false)
	@Enumerated(EnumType.STRING) 
	private Tipo tipo;
	@ManyToOne
	private Usuario usuario;
	@ManyToOne
	private Inscricao inscricao;
	@Column(name="Usuario_Aceito", nullable=false)
	private boolean aceiteUsuario;
	@Column(name="Suspenso_Jogos", nullable=false)
	private boolean suspensoJogos;
	@Column(name="Inscricao_Validada", nullable=false)
	private boolean inscricaoValidada;
	
	// Getters e Setters
	public long getIdInscrito() {
		return idInscrito;
	}
	public void setIdInscrito(long idInscrito) {
		this.idInscrito = idInscrito;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Inscricao getInscricao() {
		return inscricao;
	}
	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}
	public boolean isAceiteUsuario() {
		return aceiteUsuario;
	}
	public void setAceiteUsuario(boolean aceiteUsuario) {
		this.aceiteUsuario = aceiteUsuario;
	}
	public boolean isSuspensoJogos() {
		return suspensoJogos;
	}
	public void setSuspensoJogos(boolean suspensoJogos) {
		this.suspensoJogos = suspensoJogos;
	}
	public boolean isInscricaoValidada() {
		return inscricaoValidada;
	}
	public void setInscricaoValidada(boolean inscricaoValidada) {
		if(this.inscricao.isValidada()==true)
			this.inscricaoValidada = true;
		else
			this.inscricaoValidada=false;
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (aceiteUsuario ? 1231 : 1237);
		result = prime * result + (int) (idInscrito ^ (idInscrito >>> 32));
		result = prime * result + ((inscricao == null) ? 0 : inscricao.hashCode());
		result = prime * result + (inscricaoValidada ? 1231 : 1237);
		result = prime * result + (suspensoJogos ? 1231 : 1237);
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inscrito other = (Inscrito) obj;
		if (aceiteUsuario != other.aceiteUsuario)
			return false;
		if (idInscrito != other.idInscrito)
			return false;
		if (inscricao == null) {
			if (other.inscricao != null)
				return false;
		}
		else if (!inscricao.equals(other.inscricao))
			return false;
		if (inscricaoValidada != other.inscricaoValidada)
			return false;
		if (suspensoJogos != other.suspensoJogos)
			return false;
		if (tipo != other.tipo)
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		}
		else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Inscrito [idInscrito=" + idInscrito + ", tipo=" + tipo + ", usuario=" + usuario + ", inscricao="
				+ inscricao + ", aceiteUsuario=" + aceiteUsuario + ", suspensoJogos=" + suspensoJogos
				+ ", inscricaoValidada=" + inscricaoValidada + "]";
	}
}
