package sistema.modelos;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Gol")
public class Gol implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idGol;
	@ManyToOne
	private Inscrito inscrito;
	@Column(name="Tempo", nullable=false)
	private int tempo;
	@Column(name="Penalty")
	private boolean penalty;
	
	// Getters e Setters
	public int getIdGol() {
		return idGol;
	}
	public void setIdGol(int idGol) {
		this.idGol = idGol;
	}
	public Inscrito getInscrito() {
		return inscrito;
	}
	public void setInscrito(Inscrito inscrito) {
		this.inscrito = inscrito;
	}
	public int getTempo() {
		return tempo;
	}
	public void setTempo(int tempo) {
		this.tempo = tempo;
	}
	public boolean isPenalty() {
		return penalty;
	}
	public void setPenalty(boolean penalty) {
		this.penalty = penalty;
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idGol;
		result = prime * result + ((inscrito == null) ? 0 : inscrito.hashCode());
		result = prime * result + (penalty ? 1231 : 1237);
		result = prime * result + tempo;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gol other = (Gol) obj;
		if (idGol != other.idGol)
			return false;
		if (inscrito == null) {
			if (other.inscrito != null)
				return false;
		}
		else if (!inscrito.equals(other.inscrito))
			return false;
		if (penalty != other.penalty)
			return false;
		if (tempo != other.tempo)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Gol [idGol=" + idGol + ", inscrito=" + inscrito + ", tempo=" + tempo + ", penalty=" + penalty + "]";
	}
}