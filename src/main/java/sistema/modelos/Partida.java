package sistema.modelos;

import java.util.List;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="Partida")
public class Partida implements Serializable
{
	// Atributos
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int numero;
	@ManyToOne
	private Inscricao equipeMandante;
	@ManyToOne
	private Inscricao equipeVisitante;
	@Column(name="Data", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date data;
	@ManyToOne
	private Local local;
	@OneToOne
	private Partida proxPartida;
	@OneToMany
	private List <Juiz> juizes;
	@ManyToOne
	private Grupo grupo;
	@Column(name="Relato_Juiz")
	private String relatoJuiz;
	
	// Getters e Setters
	public Inscricao getEquipeMandante() {
		return equipeMandante;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public void setEquipeMandante(Inscricao equipeMandante) {
		this.equipeMandante = equipeMandante;
	}
	public Inscricao getEquipeVisitante() {
		return equipeVisitante;
	}
	public void setEquipeVisitante(Inscricao equipeVisitante) {
		this.equipeVisitante = equipeVisitante;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Local getLocal() {
		return local;
	}
	public void setLocal(Local local) {
		this.local = local;
	}
	public Partida getProxPartida() {
		return proxPartida;
	}
	public void setProxPartida(Partida proxPartida) {
		this.proxPartida = proxPartida;
	}
	public List<Juiz> getJuizes() {
		return juizes;
	}
	public void setJuizes(List<Juiz> juizes) {
		this.juizes = juizes;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public String getRelatoJuiz() {
		return relatoJuiz;
	}
	public void setRelatoJuiz(String relatoJuiz) {
		this.relatoJuiz = relatoJuiz;
	}
	
	//Add's
	public void addJuiz(Juiz juiz)
	{
		juizes.add(juiz);
	}
	
	// M�todos
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((equipeMandante == null) ? 0 : equipeMandante.hashCode());
		result = prime * result + ((equipeVisitante == null) ? 0 : equipeVisitante.hashCode());
		result = prime * result + ((grupo == null) ? 0 : grupo.hashCode());
		result = prime * result + ((juizes == null) ? 0 : juizes.hashCode());
		result = prime * result + ((local == null) ? 0 : local.hashCode());
		result = prime * result + numero;
		result = prime * result + ((proxPartida == null) ? 0 : proxPartida.hashCode());
		result = prime * result + ((relatoJuiz == null) ? 0 : relatoJuiz.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Partida other = (Partida) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		}
		else if (!data.equals(other.data))
			return false;
		if (equipeMandante == null) {
			if (other.equipeMandante != null)
				return false;
		}
		else if (!equipeMandante.equals(other.equipeMandante))
			return false;
		if (equipeVisitante == null) {
			if (other.equipeVisitante != null)
				return false;
		}
		else if (!equipeVisitante.equals(other.equipeVisitante))
			return false;
		if (grupo == null) {
			if (other.grupo != null)
				return false;
		}
		else if (!grupo.equals(other.grupo))
			return false;
		if (juizes == null) {
			if (other.juizes != null)
				return false;
		}
		else if (!juizes.equals(other.juizes))
			return false;
		if (local == null) {
			if (other.local != null)
				return false;
		} else if (!local.equals(other.local))
			return false;
		if (numero != other.numero)
			return false;
		if (proxPartida == null) {
			if (other.proxPartida != null)
				return false;
		}
		else if (!proxPartida.equals(other.proxPartida))
			return false;
		if (relatoJuiz == null) {
			if (other.relatoJuiz != null)
				return false;
		}
		else if (!relatoJuiz.equals(other.relatoJuiz))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Partida [numero=" + numero + ", equipeMandante=" + equipeMandante + ", equipeVisitante="
				+ equipeVisitante + ", data=" + data + ", local=" + local + ", proxPartida=" + proxPartida + ", juizes="
				+ juizes + ", grupo=" + grupo + ", relatoJuiz=" + relatoJuiz + "]";
	}
}
