package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import sistema.modelos.Campeonato;
import sistema.modelos.Juiz;
import sistema.service.CampeonatoService;
import sistema.service.JuizService;

@ManagedBean
@SessionScoped
public class JuizManagedBeans 
{
	// Atributos
	private Juiz juiz = new Juiz();
	private List<Juiz> juizes;
	private JuizService service = new JuizService();
	private CampeonatoService campService = new CampeonatoService();
	private Campeonato campeonatoSelecionado;
	
	//Getters e Setters
	public Juiz getJuiz() {
		return juiz;
	}
	public void setJuiz(Juiz juiz) {
		this.juiz = juiz;
	}
	public List<Juiz> getJuizes() {
		if (juizes == null)
			juizes = service.getJuizes();
		return juizes;
	}
	public Campeonato getCampeonatoSelecionado() {
		return campeonatoSelecionado;
	}
	public void setCampeonatoSelecionado(Campeonato campeonatoSelecionado) {
		this.campeonatoSelecionado = campeonatoSelecionado;
	}

	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Juiz juiz = ((Juiz) event.getObject());
		service.alterar(juiz);
	}
	public void salvar() {
		campeonatoSelecionado.addJuiz(juiz);
		service.salvar(juiz);
		campService.alterar(campeonatoSelecionado);
		//if (juizes != null)
			//juizes.add(juiz);
		juiz = new Juiz();
	}
	public void remover(Juiz juiz) {
		campeonatoSelecionado.getJuizes().remove(juiz);
		campService.alterar(campeonatoSelecionado);
	}
}
