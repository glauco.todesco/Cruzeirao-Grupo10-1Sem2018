package sistema.beans.datamodel;

import java.util.List;

import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

import sistema.modelos.Fase;
import sistema.service.FaseService;

public class FaseDataModel extends ListDataModel<Fase> implements SelectableDataModel<Fase>
{
	private FaseService service = new FaseService();

	public FaseDataModel() {
		
	}

	public FaseDataModel(List <Fase> list) {
		super(list);
	}
	
	@Override
	public Fase getRowData(String rowKey) {
		for(Fase f: service.getFases())
		   if(Integer.parseInt(rowKey) ==  f.getNumero())
			   return service.pesquisar(f);
		
		return null;
	}
	
	@Override
	public Object getRowKey(Fase fase) {
		return fase.getNumero();
	}
}