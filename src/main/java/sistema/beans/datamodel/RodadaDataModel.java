package sistema.beans.datamodel;

import java.util.List;

import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

import sistema.modelos.Rodada;
import sistema.service.RodadaService;

public class RodadaDataModel extends ListDataModel<Rodada> implements SelectableDataModel<Rodada>
{
	private RodadaService service = new RodadaService();

	public RodadaDataModel() {
		
	}

	public RodadaDataModel(List <Rodada> list) {
		super(list);
	}
	
	@Override
	public Rodada getRowData(String rowKey) {
		for(Rodada r: service.getRodadas())
		   if(Integer.parseInt(rowKey) ==  r.getNumero())
			   return service.pesquisar(r);
		
		return null;
	}
	
	@Override
	public Object getRowKey(Rodada rodada) {
		return rodada.getNumero();
	}
}