package sistema.beans.datamodel;

import java.util.List;

import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

import sistema.modelos.Grupo;
import sistema.service.GrupoService;

public class GrupoDataModel extends ListDataModel<Grupo> implements SelectableDataModel<Grupo>
{
	private GrupoService service = new GrupoService();

	public GrupoDataModel() {
		
	}

	public GrupoDataModel(List <Grupo> list) {
		super(list);
	}
	
	@Override
	public Grupo getRowData(String rowKey) {
		for(Grupo g: service.getGrupos())
		   if(Integer.parseInt(rowKey) ==  g.getNumero())
			   return service.pesquisar(g);
		
		return null;
	}
	
	@Override
	public Object getRowKey(Grupo grupo) {
		return grupo.getNumero();
	}
}