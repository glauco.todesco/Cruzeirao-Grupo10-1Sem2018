package sistema.beans.datamodel;

import java.util.List;

import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

import sistema.modelos.Partida;
import sistema.service.PartidaService;

public class PartidaDataModel extends ListDataModel<Partida> implements SelectableDataModel<Partida>
{
	private PartidaService service = new PartidaService();

	public PartidaDataModel() {
		
	}

	public PartidaDataModel(List <Partida> list) {
		super(list);
	}
	
	@Override
	public Partida getRowData(String rowKey) {
		for(Partida p: service.getPartidas())
		   if(Integer.parseInt(rowKey) ==  p.getNumero())
			   return service.pesquisar(p);
		
		return null;
	}
	
	@Override
	public Object getRowKey(Partida partida) {
		return partida.getNumero();
	}
}