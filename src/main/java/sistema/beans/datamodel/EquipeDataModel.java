package sistema.beans.datamodel;

import java.util.List;

import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

import sistema.modelos.Equipe;
import sistema.service.EquipeService;

public class EquipeDataModel extends ListDataModel<Equipe> implements SelectableDataModel<Equipe>
{
	private EquipeService service = new EquipeService();

	public EquipeDataModel() {
		
	}

	public EquipeDataModel(List <Equipe> list) {
		super(list);
	}
	
	@Override
	public Equipe getRowData(String rowKey) {
		for(Equipe e: service.getEquipes())
		   if(Integer.parseInt(rowKey) ==  e.getIdEquipe())
			   return service.pesquisar(e);
		
		return null;
	}
	
	@Override
	public Object getRowKey(Equipe equipe) {
		return equipe.getIdEquipe();
	}
}