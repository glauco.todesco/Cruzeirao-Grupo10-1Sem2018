package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;

import org.primefaces.event.RowEditEvent;

import sistema.beans.datamodel.GrupoDataModel;
import sistema.modelos.Fase;
import sistema.modelos.Grupo;
import sistema.service.FaseService;
import sistema.service.GrupoService;

@ManagedBean
@SessionScoped
public class GrupoManagedBeans 
{
	// Atributos
	private Grupo grupo = new Grupo();
	private Grupo grupoSelecionado;
	private List<Grupo> grupos;
	private List<Grupo> gruposFase;
	private GrupoService service = new GrupoService();
	private FaseService faseService = new FaseService();
	private Fase faseSelecionada;
	
	//Getters e Setters
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public DataModel<Grupo> getGrupos() {
		if (grupos == null)
			grupos = service.getGrupos();
		return new GrupoDataModel(grupos);
	}
	public DataModel<Grupo> getGruposFase()
	{
		return new GrupoDataModel(faseSelecionada.getGrupos());
	}
	public Grupo getGrupoSelecionado() {
		return grupoSelecionado;
	}
	public void setGrupoSelecionado(Grupo grupoSelecionado) {
		this.grupoSelecionado = grupoSelecionado;
	}
	public Fase getFaseSelecionada() {
		return faseSelecionada;
	}
	public void setFaseSelecionada(Fase faseSelecionada) {
		this.faseSelecionada = faseSelecionada;
	}
	
	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Grupo grupo = ((Grupo) event.getObject());
		service.alterar(grupo);
	}
	public void salvar() {
		faseSelecionada.addGrupo(grupo);
		service.salvar(grupo);
		faseService.alterar(faseSelecionada);
		if(grupos != null)
			grupos.add(grupo);
		grupo.setFase(faseSelecionada);
		service.alterar(grupo);
		grupo = new Grupo();
	}
	public void remover(Grupo grupo) {
		faseSelecionada.getGrupos().remove(grupo);
		grupo.setFase(null);
		faseService.alterar(faseSelecionada);
		service.alterar(grupo);
	}
}
