package sistema.beans;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;

import org.primefaces.event.RowEditEvent;
import sistema.beans.datamodel.CategoriaDataModel;
import sistema.modelos.Campeonato;
import sistema.modelos.Categoria;
import sistema.service.CampeonatoService;
import sistema.service.CategoriaService;
import Enums.Sexo;

@ManagedBean
@SessionScoped
public class CategoriaManagedBean 
{
	// Atributos
	private Categoria categoria = new Categoria();
	private List<Categoria> categorias;
	private List<Categoria> categoriasCampeonato;
	private CategoriaService service = new CategoriaService();
	private List<Sexo> sexo;
	private CampeonatoService campService = new CampeonatoService();
	private Campeonato campeonatoSelecionado;
	private Categoria categoriaSelecionada;
	
	//Construtores
	public CategoriaManagedBean() {
		sexo = Arrays.asList(Sexo.values());
	}
	
	// Getters e Setters
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public DataModel<Categoria> getCategorias() {
		if (categorias == null)
			categorias = service.getCategorias();
		return new CategoriaDataModel(categorias);
	}
	public DataModel<Categoria> getCategoriasCampeonato()
	{
		return new CategoriaDataModel(campeonatoSelecionado.getCategorias());
	}
	public List<Sexo> getSexo() {
		return sexo;
	}
	public Campeonato getCampeonatoSelecionado() {
		return campeonatoSelecionado;
	}
	public void setCampeonatoSelecionado(Campeonato campeonatoSelecionado) {
		this.campeonatoSelecionado = campeonatoSelecionado;
	}
	public Categoria getCategoriaSelecionada() {
		return categoriaSelecionada;
	}
	public void setCategoriaSelecionada(Categoria categoriaSelecionada) {
		this.categoriaSelecionada = categoriaSelecionada;
	}

	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Categoria categoria = ((Categoria) event.getObject());
		service.alterar(categoria);
	}
	public void salvar() {
		campeonatoSelecionado.addCategoria(categoria);
		service.salvar(categoria);
		campService.alterar(campeonatoSelecionado);
		if (categorias != null)
			categorias.add(categoria);
		categoria.setCampeonato(campeonatoSelecionado);
		service.alterar(categoria);
		categoria = new Categoria();
	}
	public void remover(Categoria categoria) {
		campeonatoSelecionado.getCategorias().remove(categoria);
		categoria.setCampeonato(null);
		campService.alterar(campeonatoSelecionado);
		service.alterar(categoria);
	}
}
