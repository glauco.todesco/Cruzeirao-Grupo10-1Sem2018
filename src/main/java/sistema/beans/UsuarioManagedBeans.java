package sistema.beans;

import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import org.primefaces.event.RowEditEvent;
import sistema.beans.datamodel.UsuarioDataModel;
import sistema.modelos.Usuario;
import sistema.modelos.Equipe;
import sistema.modelos.Inscrito;
import sistema.modelos.Campeonato;
import sistema.service.UsuarioService;
import Enums.Tipo;
import Enums.Sexo;

@ManagedBean
@ViewScoped
public class UsuarioManagedBeans 
{
	// Atributos
	private Usuario usuario = new Usuario();
	private Usuario usuarioSelecionado;
	private UsuarioService service = new UsuarioService();
	private List<Usuario> usuarios;
	private List<Usuario> juizes;
	private List<Usuario> diretores;
	private List<Tipo> tipo;
	private List<Sexo> sexo;
	private String userCPF;
	private Equipe equipeSelecionada;
	
	//Construtores
	public UsuarioManagedBeans() {
		tipo = Arrays.asList(Tipo.values());
		sexo = Arrays.asList(Sexo.values());
	}
	
	//Getters e Setters
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public List<Usuario> getUsuarios() {
		if (usuarios == null)
			usuarios = service.getUsuarios();
		return usuarios;
	}
	public List<Usuario> getJuizes()
	{
		if(juizes == null)
			juizes = service.getAllJuizes();
		return juizes;
	}
	public List<Usuario> getDiretores()
	{
		if(diretores == null)
			diretores = service.getAllDiretores();
		return diretores;
	}
	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}
	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = service.pesquisar(usuarioSelecionado);
	}
	public List<Equipe> getEquipesUsuario(){
		if(usuarioSelecionado != null)
			return service.pesquisarEquipesUsuario(usuarioSelecionado);
		else
			return null;
	}
	public List<Inscrito> getInscricoesUsuario(){
		if(usuarioSelecionado != null)
			return service.pesquisarInscritosUsuario(usuarioSelecionado);
		else
			return null;
	}
	public List<Campeonato> getCampeonatosUsuario(){
		if(usuarioSelecionado != null)
			return service.pesquisarCampeonatosUsuario(usuarioSelecionado);
		else
			return null;
	}
	public List<Tipo> getTipo() {
		return tipo;
	}
	public List<Sexo> getSexo() {
		return sexo;
	}
	public String getUserCPF() {
		return userCPF;
	}
	public void setUserCPF(String userCPF) {
		this.userCPF = userCPF;
	}
	public Equipe getEquipeSelecionada() {
		return equipeSelecionada;
	}
	public void setEquipeSelecionada(Equipe equipeSelecionada) {
		this.equipeSelecionada = equipeSelecionada;
	}

	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Usuario usuario = ((Usuario) event.getObject());
		service.alterar(usuario);
	}
	public void salvar() {
		usuario = service.salvar(usuario);
		if (usuarios != null)
			usuarios.add(usuario);
		usuario = new Usuario();
	}
	public void remover(Usuario usuario) {
		if(service.pesquisarInscritosUsuario(usuario).size()>0)
		{
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "N�o � poss�vel remover usu�rio",
					"Usu�rio possui inscri��es!"));
		}
		else if(service.pesquisarEquipesUsuario(usuario).size()>0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "N�o � poss�vel remover usu�rio",
					"Usu�rio possui equipes!"));
		}
		else
		{
			service.remover(usuario);
			usuarios.remove(usuario);
		}
	}
	public void buscaporcpf()
	{
		usuarioSelecionado = new Usuario();
		List <Usuario> list = this.getUsuarios();
		for(Usuario u: list)
		{
			if(u.getCpf().equals(this.userCPF))
				this.usuarioSelecionado = u;
		}
	}
	public void boleto(Usuario usuario)
	{
		List <Inscrito> inscricoes = this.getInscricoesUsuario();
		for(Inscrito i: inscricoes)
		{
			if(i.isAceiteUsuario() == false)
				i.setAceiteUsuario(true);
			
			if(i.isInscricaoValidada() == false)
				i.setInscricaoValidada(true);
		}
		System.out.println("Usu�rio validou inscri��o(�es)");
	}
}