package sistema.beans;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import sistema.modelos.Cartao;
import sistema.service.CartaoService;
import Enums.TipoCartao;

@ManagedBean
@SessionScoped
public class CartaoManagedBean 
{
	// Atributos
	private Cartao cartao = new Cartao();
	private List<Cartao> cartoes;
	private CartaoService service = new CartaoService();
	private List<TipoCartao> tipoCartao;
	
	//Construtores
	public CartaoManagedBean() {
		tipoCartao = Arrays.asList(TipoCartao.values());
	}
	
	// Getters e Setters
	public Cartao getCartao() {
		return cartao;
	}
	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}
	public List<Cartao> getCartoes() {
		if (cartoes == null)
			cartoes = service.getCartoes();
		return cartoes;
	}
	public List<TipoCartao> getTipoCartao() {
		return tipoCartao;
	}

	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Cartao cartao = ((Cartao) event.getObject());
		service.alterar(cartao);
	}
	public void salvar() {
		service.salvar(cartao);
		if (cartoes != null)
			cartoes.add(cartao);
		cartao = new Cartao();
	}
	public void remover(Cartao cartao) {
		service.remover(cartao);
		cartoes.remove(cartao);
	}
}
