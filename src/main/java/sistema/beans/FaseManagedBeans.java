package sistema.beans;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;

import org.primefaces.event.RowEditEvent;

import sistema.beans.datamodel.FaseDataModel;
import sistema.modelos.Categoria;
import sistema.modelos.Fase;
import sistema.service.CategoriaService;
import sistema.service.FaseService;
import Enums.FormatoFase;

@ManagedBean
@SessionScoped
public class FaseManagedBeans 
{
	// Atributos
	private Fase fase = new Fase();
	private Fase faseSelecionada;
	private List<Fase> fases;
	private List<Fase> fasesCategoria;
	private FaseService service = new FaseService();
	private List<FormatoFase> formatoFase;
	private CategoriaService catService = new CategoriaService();
	private Categoria categoriaSelecionada;
	
	//Construtores
	public FaseManagedBeans() {
		formatoFase = Arrays.asList(FormatoFase.values());
	}
	
	// Getters e Setters
	public Fase getFase() {
		return fase;
	}
	public void setFase(Fase fase) {
		this.fase = fase;
	}
	public DataModel<Fase> getFases() {
		if (fases == null)
			fases = service.getFases();
		return new FaseDataModel(fases);
	}
	public DataModel<Fase> getFasesCategoria()
	{
		return new FaseDataModel(categoriaSelecionada.getFases());
	}
	public List<FormatoFase> getFormatoFase() {
		return formatoFase;
	}
	public Categoria getCategoriaSelecionada() {
		return categoriaSelecionada;
	}
	public void setCategoriaSelecionada(Categoria categoriaSelecionada) {
		this.categoriaSelecionada = categoriaSelecionada;
	}
	public Fase getFaseSelecionada() {
		return faseSelecionada;
	}
	public void setFaseSelecionada(Fase faseSelecionada) {
		this.faseSelecionada = faseSelecionada;
	}

	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Fase fase = ((Fase) event.getObject());
		service.alterar(fase);
	}
	public void salvar() {
		categoriaSelecionada.addFase(fase);
		service.salvar(fase);
		catService.alterar(categoriaSelecionada);
		if(fases != null)
			fases.add(fase);
		fase.setCategoria(categoriaSelecionada);
		service.alterar(fase);
		fase = new Fase();
	}
	public void remover(Fase fase) {
		categoriaSelecionada.getFases().remove(fase);
		fase.setCategoria(null);
		catService.alterar(categoriaSelecionada);
		service.alterar(fase);
	}
}
