package sistema.beans;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import sistema.modelos.Inscrito;
import sistema.service.InscritoService;
import Enums.Tipo;

@ManagedBean
@SessionScoped
public class InscritoManagedBeans 
{
	// Atributos
	private Inscrito inscrito = new Inscrito();
	private List<Inscrito> inscritos;
	private InscritoService service = new InscritoService();
	private List<Tipo> tipo;
	
	//Construtores
	public InscritoManagedBeans(List<Tipo> tipo) {
		tipo = Arrays.asList(Tipo.values());
	}
	
	//Getters e Setters
	public Inscrito getInscrito() {
		return inscrito;
	}
	public void setInscrito(Inscrito inscrito) {
		this.inscrito = inscrito;
	}
	public List<Inscrito> getInscritos() {
		if (inscritos == null)
			inscritos = service.getInscritos();
		return inscritos;
	}
	public List<Tipo> getTipo() {
		return tipo;
	}

	//M�todos
	public void onRowEdit(RowEditEvent event) {
		Inscrito inscrito = ((Inscrito) event.getObject());
		service.alterar(inscrito);
	}
	public void salvar() {
		service.salvar(inscrito);
		if (inscritos != null)
			inscritos.add(inscrito);
		inscrito = new Inscrito();
	}
	public void remover(Inscrito inscrito) {
		service.remover(inscrito);
		inscritos.remove(inscrito);
	}
}
