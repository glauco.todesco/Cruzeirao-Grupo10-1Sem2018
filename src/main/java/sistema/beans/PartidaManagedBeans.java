package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import sistema.modelos.Partida;
import sistema.modelos.Rodada;
import sistema.service.PartidaService;
import sistema.service.RodadaService;

@ManagedBean
@SessionScoped
public class PartidaManagedBeans 
{
	// Atributos
	private Partida partida = new Partida();
	private List<Partida> partidas;
	private PartidaService service = new PartidaService();
	private RodadaService rodService = new RodadaService();
	private Rodada rodadaSelecionada;
	
	//Getters e Setters
	public Partida getPartida() {
		return partida;
	}
	public void setPartida(Partida partida) {
		this.partida = partida;
	}
	public List<Partida> getPartidas() {
		if (partidas == null)
			partidas = service.getPartidas();
		return partidas;
	}
	public Rodada getRodadaSelecionada() {
		return rodadaSelecionada;
	}
	public void setRodadaSelecionada(Rodada rodadaSelecionada) {
		this.rodadaSelecionada = rodadaSelecionada;
	}
	
	//M�todos
	public void onRowEdit(RowEditEvent event) {
		Partida partida = ((Partida) event.getObject());
		service.alterar(partida);
	}
	public void salvar() {
		rodadaSelecionada.addPartida(partida);
		service.salvar(partida);
		rodService.alterar(rodadaSelecionada);
		if(partidas != null)
			partidas.add(partida);
		partida.setGrupo(rodadaSelecionada.getGrupo());
		service.alterar(partida);
		partida = new Partida();
	}
	public void remover(Partida partida) {
		rodadaSelecionada.getPartidas().remove(partida);
		partida.setGrupo(null);
		rodService.alterar(rodadaSelecionada);
		service.alterar(partida);
	}
}
