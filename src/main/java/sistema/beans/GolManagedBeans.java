package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import sistema.modelos.Gol;
import sistema.service.GolService;

@ManagedBean
@SessionScoped
public class GolManagedBeans 
{
	// Atributos
	private Gol gol = new Gol();
	private List<Gol> gols;
	private GolService service = new GolService();
	
	// Getters e Setters
	public Gol getGol() {
		return gol;
	}
	public void setGol(Gol gol) {
		this.gol = gol;
	}
	public List<Gol> getGols() {
		if (gols == null)
			gols = service.getGols();
		return gols;
	}
	
	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Gol gol = ((Gol) event.getObject());
		service.alterar(gol);
	}
	public void salvar() {
		service.salvar(gol);
		if (gols != null)
			gols.add(gol);
		gol = new Gol();
	}
	public void remover(Gol gol) {
		service.remover(gol);
		gols.remove(gol);
	}
}
