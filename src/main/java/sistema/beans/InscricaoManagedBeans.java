package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import sistema.modelos.Categoria;
import sistema.modelos.Inscricao;
import sistema.service.CategoriaService;
import sistema.service.InscricaoService;

@ManagedBean
@SessionScoped
public class InscricaoManagedBeans 
{
	// Atributos
	private Inscricao inscricao = new Inscricao();
	private List<Inscricao> inscricoes;
	private List<Inscricao> inscricoesCategoria;
	private InscricaoService service = new InscricaoService();
	private CategoriaService catService = new CategoriaService();
	private Categoria categoriaSelecionada = new Categoria();
	
	// Getters e Setters
	public Inscricao getInscricao() {
		return inscricao;
	}
	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}
	public List<Inscricao> getInscricoes() {
		if (inscricoes == null)
			inscricoes = service.getInscricoes();
		return inscricoes;
	}
	public List<Inscricao> getInscricoesCategoria()
	{
		return categoriaSelecionada.getInscricoes();
	}
	public Categoria getCategoriaSelecionada() {
		return categoriaSelecionada;
	}
	public void setCategoriaSelecionada(Categoria categoriaSelecionada) {
		this.categoriaSelecionada = categoriaSelecionada;
	}
	
	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Inscricao inscricao = ((Inscricao) event.getObject());
		service.alterar(inscricao);
	}
	public void salvar() {
		categoriaSelecionada.addInscricao(inscricao);
		service.salvar(inscricao);
		catService.alterar(categoriaSelecionada);
		if(inscricoes != null)
			inscricoes.add(inscricao);
		inscricao.setCategoria(categoriaSelecionada);
		service.alterar(inscricao);
		inscricao = new Inscricao();
	}
	public void remover(Inscricao inscricao) {
		categoriaSelecionada.getInscricoes().remove(inscricao);
		inscricao.setCategoria(null);
		catService.alterar(categoriaSelecionada);
		service.alterar(inscricao);
	}
}
