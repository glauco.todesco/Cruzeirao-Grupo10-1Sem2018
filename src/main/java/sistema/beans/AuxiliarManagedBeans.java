package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import sistema.modelos.Auxiliar;
import sistema.service.AuxiliarService;

@ManagedBean
@SessionScoped
public class AuxiliarManagedBeans 
{
	// Atributos
	private Auxiliar aux = new Auxiliar();
	private AuxiliarService service=new AuxiliarService();
	private List <Auxiliar> listaAux=service.teste();
	
	// Getters e Setters
	public Auxiliar getAux() {
		return aux;
	}
	public void setAux(Auxiliar aux) {
		this.aux = aux;
	}
	public AuxiliarService getService() {
		return service;
	}
	public void setService(AuxiliarService service) {
		this.service = service;
	}
	public List<Auxiliar> getListaAux() {
		return listaAux;
	}
	public void setListaAux(List<Auxiliar> listaAux) {
		this.listaAux = listaAux;
	}
}
