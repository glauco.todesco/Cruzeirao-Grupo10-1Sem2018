package sistema.beans;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import org.primefaces.event.RowEditEvent;
import sistema.beans.datamodel.CampeonatoDataModel;
import sistema.beans.datamodel.CategoriaDataModel;
import sistema.modelos.Campeonato;
import sistema.modelos.Local;
import sistema.modelos.Juiz;
import sistema.modelos.Categoria;
import sistema.service.CampeonatoService;

@ManagedBean
@ViewScoped
public class CampeonatoManagedBean 
{
	// Atributos
	private Campeonato campeonato = new Campeonato();
	private Campeonato campeonatoSelecionado;
	private CampeonatoService service = new CampeonatoService();
	private List<Campeonato> campeonatos;
	
	// Getters e Setters
	public Campeonato getCampeonato() {
		return campeonato;
	}
	public void setCampeonato(Campeonato campeonato) {
		this.campeonato = campeonato;
	}
	public DataModel<Campeonato> getCampeonatos() {
		if (campeonatos == null)
			campeonatos = service.getCampeonatos();
		return new CampeonatoDataModel(campeonatos);
	}
	public Campeonato getCampeonatoSelecionado() {
		return campeonatoSelecionado;
	}
	public void setCampeonatoSelecionado(Campeonato campeonatoSelecionado) {
		this.campeonatoSelecionado = service.pesquisar(campeonatoSelecionado);
	}
	public List<Local> getLocaisCampeonato(){
		if(campeonatoSelecionado != null)
			return service.pesquisarLocaisCampeonato(campeonatoSelecionado);
		else
			return null;
	}
	public List<Juiz> getJuizesCampeonato(){
		if(campeonatoSelecionado != null)
			return service.pesquisarJuizesCampeonato(campeonatoSelecionado);
		else
			return null;
	}
	public DataModel<Categoria> getCategoriasCampeonato(){
		if(campeonatoSelecionado != null)
			return new CategoriaDataModel(service.pesquisarCategoriasCampeonato(campeonatoSelecionado));
		else
			return null;
	}
	
	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Campeonato campeonato = ((Campeonato) event.getObject());
		service.alterar(campeonato);
	}
	public void salvar() {
		campeonato = service.salvar(campeonato);
		if (campeonatos != null)
			campeonatos.add(campeonato);
		campeonato = new Campeonato();
	}
	public void remover(Campeonato campeonato) {
		if(service.pesquisarCategoriasCampeonato(campeonato).size()>0)
		{
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "N�o � poss�vel remover campeonato",
					"Campeonato possui categorias!"));
		}
		else
		{
			service.remover(campeonato);
			campeonatos.remove(campeonato);
		}
	}
}