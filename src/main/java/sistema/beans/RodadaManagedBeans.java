package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;

import org.primefaces.event.RowEditEvent;

import sistema.beans.datamodel.RodadaDataModel;
import sistema.modelos.Grupo;
import sistema.modelos.Rodada;
import sistema.service.GrupoService;
import sistema.service.RodadaService;

@ManagedBean
@SessionScoped
public class RodadaManagedBeans 
{
	// Atributos
	private Rodada rodada = new Rodada();
	private Rodada rodadaSelecionada;
	private List<Rodada> rodadas;
	private List<Rodada> rodadasGrupo;
	private RodadaService service = new RodadaService();
	private GrupoService grupoService = new GrupoService();
	private Grupo grupoSelecionado;
	
	//Getters e Setters
	public Rodada getRodada() {
		return rodada;
	}
	public void setRodada(Rodada rodada) {
		this.rodada = rodada;
	}
	public DataModel<Rodada> getRodadas() {
		if (rodadas == null)
			rodadas = service.getRodadas();
		return new RodadaDataModel(rodadas);
	}
	public DataModel<Rodada> getRodadasGrupo()
	{
		return new RodadaDataModel(grupoSelecionado.getRodadas());
	}
	public Rodada getRodadaSelecionada() {
		return rodadaSelecionada;
	}
	public void setRodadaSelecionada(Rodada rodadaSelecionada) {
		this.rodadaSelecionada = rodadaSelecionada;
	}
	public Grupo getGrupoSelecionado() {
		return grupoSelecionado;
	}
	public void setGrupoSelecionado(Grupo grupoSelecionado) {
		this.grupoSelecionado = grupoSelecionado;
	}
	
	//M�todos
	public void onRowEdit(RowEditEvent event) {
		Rodada rodada = ((Rodada) event.getObject());
		service.alterar(rodada);
	}
	public void salvar() {
		grupoSelecionado.addRodada(rodada);
		service.salvar(rodada);
		grupoService.alterar(grupoSelecionado);
		if(rodadas != null)
			rodadas.add(rodada);
		rodada.setGrupo(grupoSelecionado);
		service.alterar(rodada);
		rodada = new Rodada();
	}
	public void remover(Rodada rodada) {
		grupoSelecionado.getRodadas().remove(rodada);
		rodada.setGrupo(null);
		grupoService.alterar(grupoSelecionado);
		service.alterar(rodada);
	}
}