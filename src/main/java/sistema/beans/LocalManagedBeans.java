package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import sistema.modelos.Local;
import sistema.modelos.Campeonato;
import sistema.service.CampeonatoService;
import sistema.service.LocalService;

@ManagedBean
@SessionScoped
public class LocalManagedBeans 
{
	// Atributos
	private Local local = new Local();
	private List<Local> locais;
	private LocalService service = new LocalService();
	private CampeonatoService campService = new CampeonatoService();
	private Campeonato campeonatoSelecionado;
	
	// Getters e Setters
	public Local getLocal() {
		return local;
	}
	public void setLocal(Local local) {
		this.local = local;
	}
	public List<Local> getLocais() {
		if (locais == null)
			locais = service.getLocais();
		return locais;
	}
	public Campeonato getCampeonatoSelecionado() {
		return campeonatoSelecionado;
	}
	public void setCampeonatoSelecionado(Campeonato campeonatoSelecionado) {
		this.campeonatoSelecionado = campeonatoSelecionado;
	}
	
	//M�todos
	public void onRowEdit(RowEditEvent event) {
		Local local = ((Local) event.getObject());
		service.alterar(local);
	}
	public void salvar() {
		campeonatoSelecionado.addLocal(local);
		service.salvar(local);
		campService.alterar(campeonatoSelecionado);
		if (locais != null)
			locais.add(local);
		local = new Local();
	}
	public void remover(Local local) {
		campeonatoSelecionado.getLocais().remove(local);
		campService.alterar(campeonatoSelecionado);
	}
}
