package sistema.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;

import org.primefaces.event.RowEditEvent;

import sistema.beans.datamodel.EquipeDataModel;
import sistema.modelos.Equipe;
import sistema.modelos.Usuario;
import sistema.service.EquipeService;

@ManagedBean
@SessionScoped
public class EquipeManagedBean 
{
	// Atributos
	private Equipe equipe = new Equipe();
	private Equipe equipeSelecionada;
	private Usuario diretor = new Usuario();
	private List<Equipe> equipes;
	private List<Equipe> eqps;
	private EquipeService service = new EquipeService();
	
	//Getters e Setters
	public Equipe getEquipe() {
		return equipe;
	}
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}
	public DataModel<Equipe> getEquipes() {
		if (equipes == null)
			equipes = service.getEquipes();
		return new EquipeDataModel(equipes);
	}
	public List<Equipe> getEqps()
	{
		if(eqps == null)
			eqps = service.getEquipes();
		return eqps;
	}
	public Equipe getEquipeSelecionada() {
		return equipeSelecionada;
	}
	public void setEquipeSelecionada(Equipe equipeSelecionada) {
		this.equipeSelecionada = equipeSelecionada;
	}
	public Usuario getDiretor() {
		return diretor;
	}
	public void setDiretor(Usuario diretor) {
		this.diretor = diretor;
	}
	
	// M�todos
	public void onRowEdit(RowEditEvent event) {
		Equipe equipe = ((Equipe) event.getObject());
		service.alterar(equipe);
	}
	public void salvar() {
		service.salvar(equipe);
		if (equipes != null)
			equipes.add(equipe);
		equipe = new Equipe();
	}
	public void mostra()
	{
		System.out.println(equipes);
	}
	public void salvarDiretor()
	{
		System.out.println(equipeSelecionada);
		equipeSelecionada.addDiretor(diretor);
		service.alterar(equipeSelecionada);
		diretor = new Usuario();
	}
	public void remover(Equipe equipe) {
		service.remover(equipe);
		equipes.remove(equipe);
	}
}
