package sistema.service;

import java.util.List;

import sistema.dao.classes.LocalDAO;
import sistema.modelos.Local;

public class LocalService 
{
	LocalDAO localDAO = new LocalDAO();
	
	// M�todos
	public Local salvar(Local local) {
		local = localDAO.save(local);
		localDAO.closeEntityManager();
		return local;
	}
	public List<Local> getLocais() {
		List<Local> list = localDAO.getAll(Local.class);
		localDAO.closeEntityManager();
		return list;
	}
	public void alterar(Local local) {
		localDAO.save(local);
		localDAO.closeEntityManager();
	}
	public void remover(Local local) {
		local = localDAO.getById(Local.class, local.getIdLocal());
		localDAO.remove(local);
		localDAO.closeEntityManager();
	}
}