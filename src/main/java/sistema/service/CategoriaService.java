package sistema.service;

import java.util.List;

import sistema.dao.classes.CategoriaDAO;
import sistema.modelos.Categoria;
import sistema.modelos.Campeonato;
import sistema.modelos.Fase;
import sistema.modelos.Inscricao;

public class CategoriaService 
{
	CategoriaDAO categoriaDAO = new CategoriaDAO();
	
	// M�todos
	public Categoria salvar(Categoria categoria) {
		categoria = categoriaDAO.save(categoria);
		categoriaDAO.closeEntityManager();
		return categoria;
	}
	public List<Categoria> getCategorias() {
		List<Categoria> list = categoriaDAO.getAll(Categoria.class);
		categoriaDAO.closeEntityManager();
		return list;
	}
	public void alterar(Categoria categoria) {
		categoriaDAO.save(categoria);
		categoriaDAO.closeEntityManager();
	}
	public void remover(Categoria categoria) {
		categoria = categoriaDAO.getById(Categoria.class, categoria.getIdCategoria());
		categoriaDAO.remove(categoria);
		categoriaDAO.closeEntityManager();
	}
	public Categoria pesquisar(Categoria categoria)
	{
		categoria = categoriaDAO.getById(Categoria.class, categoria.getIdCategoria());
		categoriaDAO.closeEntityManager();
		return categoria;
	}
	public Campeonato pesquisarCampeonatoCategoria(Categoria categoria)
	{
		Campeonato campeonato;
		categoria = categoriaDAO.getById(Categoria.class, categoria.getIdCategoria());
		campeonato = categoria.getCampeonato();
		return campeonato;
	}
	public List<Inscricao> pesquisarInscricoesCategoria(Categoria categoria)
	{
		List<Inscricao> inscricoes;
		categoria = categoriaDAO.getById(Categoria.class, categoria.getIdCategoria());
		inscricoes = categoria.getInscricoes();
		return inscricoes;
	}
	public List<Fase> pesquisarFasesCategoria(Categoria categoria)
	{
		List<Fase> fases;
		categoria = categoriaDAO.getById(Categoria.class, categoria.getIdCategoria());
		fases = categoria.getFases();
		return fases;
	}
}