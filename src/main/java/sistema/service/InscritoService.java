package sistema.service;

import java.util.List;

import sistema.dao.classes.InscritoDAO;
import sistema.modelos.Inscrito;

public class InscritoService 
{
	InscritoDAO inscritoDAO = new InscritoDAO();
	
	// M�todos
	public Inscrito salvar(Inscrito inscrito) {
		inscrito = inscritoDAO.save(inscrito);
		inscritoDAO.closeEntityManager();
		return inscrito;
	}
	public List<Inscrito> getInscritos() {
		List<Inscrito> list = inscritoDAO.getAll(Inscrito.class);
		inscritoDAO.closeEntityManager();
		return list;
	}
	public void alterar(Inscrito inscrito) {
		inscritoDAO.save(inscrito);
		inscritoDAO.closeEntityManager();
	}
	public void remover(Inscrito inscrito) {
		inscrito = inscritoDAO.getById(Inscrito.class, inscrito.getIdInscrito());
		inscritoDAO.remove(inscrito);
		inscritoDAO.closeEntityManager();
	}
}