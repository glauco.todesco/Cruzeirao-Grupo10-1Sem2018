package sistema.service;

import java.util.List;

import sistema.dao.classes.JuizDAO;
import sistema.modelos.Juiz;

public class JuizService 
{
	JuizDAO juizDAO = new JuizDAO();
	
	// M�todos
	public Juiz salvar(Juiz juiz) {
		juiz = juizDAO.save(juiz);
		juizDAO.closeEntityManager();
		return juiz;
	}
	public List<Juiz> getJuizes() {
		List<Juiz> list = juizDAO.getAll(Juiz.class);
		juizDAO.closeEntityManager();
		return list;
	}
	public void alterar(Juiz juiz) {
		juizDAO.save(juiz);
		juizDAO.closeEntityManager();
	}
	public void remover(Juiz juiz) {
		juiz = juizDAO.getById(Juiz.class, juiz.getIdJuiz());
		juizDAO.remove(juiz);
		juizDAO.closeEntityManager();
	}
}