package sistema.service;

import java.util.List;

import sistema.dao.classes.RodadaDAO;
import sistema.modelos.Rodada;
import sistema.modelos.Grupo;
import sistema.modelos.Partida;

public class RodadaService 
{
	RodadaDAO rodadaDAO = new RodadaDAO();
	
	// M�todos
	public Rodada salvar(Rodada rodada) {
		rodada = rodadaDAO.save(rodada);
		rodadaDAO.closeEntityManager();
		return rodada;
	}
	public List<Rodada> getRodadas() {
		List<Rodada> list = rodadaDAO.getAll(Rodada.class);
		rodadaDAO.closeEntityManager();
		return list;
	}
	public void alterar(Rodada rodada) {
		rodadaDAO.save(rodada);
		rodadaDAO.closeEntityManager();
	}
	public void remover(Rodada rodada) {
		rodada = rodadaDAO.getById(Rodada.class, rodada.getNumero());
		rodadaDAO.remove(rodada);
		rodadaDAO.closeEntityManager();
	}
	public Rodada pesquisar(Rodada rodada)
	{
		rodada = rodadaDAO.getById(Rodada.class, rodada.getNumero());
		rodadaDAO.closeEntityManager();
		return rodada;
	}
	public Grupo pesquisarGrupoRodada(Rodada rodada)
	{
		Grupo grupo;
		rodada = rodadaDAO.getById(Rodada.class, rodada.getNumero());
		grupo = rodada.getGrupo();
		return grupo;
	}
	public List<Partida> pesquisarPartidasRodada(Rodada rodada)
	{
		List<Partida> partidas;
		rodada = rodadaDAO.getById(Rodada.class, rodada.getNumero());
		partidas=rodada.getPartidas();
		return partidas;
	}
}