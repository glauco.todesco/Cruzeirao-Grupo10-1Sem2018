package sistema.service;

import java.util.List;

import sistema.dao.classes.PartidaDAO;
import sistema.modelos.Partida;
import sistema.modelos.Local;
import sistema.modelos.Juiz;
import sistema.modelos.Grupo;

public class PartidaService 
{
	PartidaDAO partidaDAO = new PartidaDAO();
	
	// M�todos
	public Partida salvar(Partida partida) {
		partida = partidaDAO.save(partida);
		partidaDAO.closeEntityManager();
		return partida;
	}
	public List<Partida> getPartidas() {
		List<Partida> list = partidaDAO.getAll(Partida.class);
		partidaDAO.closeEntityManager();
		return list;
	}
	public void alterar(Partida partida) {
		partidaDAO.save(partida);
		partidaDAO.closeEntityManager();
	}
	public void remover(Partida partida) {
		partida = partidaDAO.getById(Partida.class, partida.getNumero());
		partidaDAO.remove(partida);
		partidaDAO.closeEntityManager();
	}
	public Partida pesquisar(Partida partida)
	{
		partida = partidaDAO.getById(Partida.class, partida.getNumero());
		partidaDAO.closeEntityManager();
		return partida;
	}
	public Local pesquisarLocalPartida(Partida partida)
	{
		Local local;
		partida = partidaDAO.getById(Partida.class, partida.getNumero());
		local = partida.getLocal();
		return local;
	}
	public List<Juiz> pesquisarJuizesPartida(Partida partida)
	{
		List<Juiz> juizes;
		partida = partidaDAO.getById(Partida.class, partida.getNumero());
		juizes = partida.getJuizes();
		return juizes;
	}
	public Grupo pesquisarGrupoPartida(Partida partida)
	{
		Grupo grupo;
		partida = partidaDAO.getById(Partida.class, partida.getNumero());
		grupo = partida.getGrupo();
		return grupo;
	}
}