package sistema.service;

import java.util.ArrayList;
import java.util.List;

import sistema.dao.classes.UsuarioDAO;
import sistema.modelos.Usuario;
import sistema.modelos.Equipe;
import sistema.modelos.Inscrito;
import sistema.modelos.Campeonato;

public class UsuarioService 
{
	UsuarioDAO usuarioDAO = new UsuarioDAO();
	
	// M�todos
	public Usuario salvar(Usuario usuario) {
		usuario = usuarioDAO.save(usuario);
		usuarioDAO.closeEntityManager();
		return usuario;
	}
	public List<Usuario> getUsuarios() {
		List<Usuario> list = usuarioDAO.getAll(Usuario.class);
		usuarioDAO.closeEntityManager();
		return list;
	}
	public void alterar(Usuario usuario) {
		usuarioDAO.save(usuario);
		usuarioDAO.closeEntityManager();
	}
	public void remover(Usuario usuario) {
		usuario = usuarioDAO.getById(Usuario.class, usuario.getIdUsuario());
		usuarioDAO.remove(usuario);
		usuarioDAO.closeEntityManager();
	}
	public Usuario pesquisar(Usuario usuario)
	{
		usuario = usuarioDAO.getById(Usuario.class, usuario.getIdUsuario());
		usuarioDAO.closeEntityManager();
		return usuario;
	}
	public List<Equipe> pesquisarEquipesUsuario(Usuario usuario)
	{
		List<Equipe> equipes;
		usuario = usuarioDAO.getById(Usuario.class, usuario.getIdUsuario());
		equipes = usuario.getEquipes();
		return equipes;
	}
	public List<Inscrito> pesquisarInscritosUsuario(Usuario usuario)
	{
		List<Inscrito> inscritos;
		usuario = usuarioDAO.getById(Usuario.class, usuario.getIdUsuario());
		inscritos = usuario.getInscricoes();
		return inscritos;
	}
	public List<Campeonato> pesquisarCampeonatosUsuario(Usuario usuario)
	{
		List<Campeonato> campeonatos;
		usuario = usuarioDAO.getById(Usuario.class, usuario.getIdUsuario());
		campeonatos = usuario.getCampeonatos();
		return campeonatos;
	}
	public List<Usuario> getAllJuizes()
	{
		List<Usuario> juizes = new ArrayList<Usuario>();
		List<Usuario> list = usuarioDAO.getAll(Usuario.class);
		for(Usuario u: list)
		{
			if(u.getTipo().getDescricao().equals("Juiz"))
				juizes.add(u);
		}
		return juizes;
	}
	public List<Usuario> getAllDiretores()
	{
		List<Usuario> diretores = new ArrayList<Usuario>();
		List<Usuario> list = usuarioDAO.getAll(Usuario.class);
		for(Usuario u: list)
		{
			if(u.getTipo().getDescricao().equals("Diretor"))
				diretores.add(u);
		}
		return diretores;
	}
}