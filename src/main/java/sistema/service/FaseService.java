package sistema.service;

import java.util.List;

import sistema.dao.classes.FaseDAO;
import sistema.modelos.Fase;
import sistema.modelos.Categoria;
import sistema.modelos.Grupo;

public class FaseService 
{
	FaseDAO faseDAO = new FaseDAO();
	
	// M�todos
	public Fase salvar(Fase fase) {
		fase = faseDAO.save(fase);
		faseDAO.closeEntityManager();
		return fase;
	}
	public List<Fase> getFases() {
		List<Fase> list = faseDAO.getAll(Fase.class);
		faseDAO.closeEntityManager();
		return list;
	}
	public void alterar(Fase fase) {
		faseDAO.save(fase);
		faseDAO.closeEntityManager();
	}
	public void remover(Fase fase) {
		fase = faseDAO.getById(Fase.class, fase.getNumero());
		faseDAO.remove(fase);
		faseDAO.closeEntityManager();
	}
	public Fase pesquisar(Fase fase)
	{
		fase = faseDAO.getById(Fase.class, fase.getNumero());
		faseDAO.closeEntityManager();
		return fase;
	}
	public Categoria pesquisarCategoriaFase(Fase fase)
	{
		Categoria categoria;
		fase = faseDAO.getById(Fase.class, fase.getNumero());
		categoria = fase.getCategoria();
		return categoria;
	}
	public List<Grupo> pesquisarGruposFase(Fase fase)
	{
		List<Grupo> grupos;
		fase = faseDAO.getById(Fase.class, fase.getNumero());
		grupos = fase.getGrupos();
		return grupos;
	}
}