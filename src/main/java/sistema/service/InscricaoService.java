package sistema.service;

import java.util.List;

import sistema.dao.classes.InscricaoDAO;
import sistema.modelos.Inscricao;
import sistema.modelos.Inscrito;
import sistema.modelos.Categoria;
import sistema.modelos.Partida;
import sistema.modelos.Equipe;

public class InscricaoService 
{
	InscricaoDAO inscricaoDAO = new InscricaoDAO();
	
	// M�todos
	public Inscricao salvar(Inscricao inscricao) {
		inscricao = inscricaoDAO.save(inscricao);
		inscricaoDAO.closeEntityManager();
		return inscricao;
	}
	public List<Inscricao> getInscricoes() {
		List<Inscricao> list = inscricaoDAO.getAll(Inscricao.class);
		inscricaoDAO.closeEntityManager();
		return list;
	}
	public void alterar(Inscricao inscricao) {
		inscricaoDAO.save(inscricao);
		inscricaoDAO.closeEntityManager();
	}
	public void remover(Inscricao inscricao) {
		inscricao = inscricaoDAO.getById(Inscricao.class, inscricao.getNumero());
		inscricaoDAO.remove(inscricao);
		inscricaoDAO.closeEntityManager();
	}
	public Inscricao pesquisar(Inscricao inscricao)
	{
		inscricao = inscricaoDAO.getById(Inscricao.class, inscricao.getNumero());
		inscricaoDAO.closeEntityManager();
		return inscricao;
	}
	public List<Inscrito> pesquisarInscritosInscricao(Inscricao inscricao)
	{
		List<Inscrito> inscritos;
		inscricao = inscricaoDAO.getById(Inscricao.class, inscricao.getNumero());
		inscritos = inscricao.getInscritos();
		return inscritos;
	}
	public Categoria pesquisarCategoriaInscricao(Inscricao inscricao)
	{
		Categoria categoria;
		inscricao = inscricaoDAO.getById(Inscricao.class, inscricao.getNumero());
		categoria = inscricao.getCategoria();
		return categoria;
	}
	public List<Partida> pesquisarPartidasInscricao(Inscricao inscricao)
	{
		List<Partida> partidas;
		inscricao = inscricaoDAO.getById(Inscricao.class, inscricao.getNumero());
		partidas = inscricao.getPartidas();
		return partidas;
	}
}