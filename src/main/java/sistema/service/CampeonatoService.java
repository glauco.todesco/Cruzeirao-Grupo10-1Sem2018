package sistema.service;

import java.util.List;

import sistema.dao.classes.CampeonatoDAO;
import sistema.modelos.Campeonato;
import sistema.modelos.Local;
import sistema.modelos.Juiz;
import sistema.modelos.Categoria;

public class CampeonatoService 
{
	CampeonatoDAO campeonatoDAO = new CampeonatoDAO();
	
	// M�todos
	public Campeonato salvar(Campeonato campeonato) {
		campeonato=campeonatoDAO.save(campeonato);
		campeonatoDAO.closeEntityManager();
		return campeonato;
	}
	public List<Campeonato> getCampeonatos() {
		List <Campeonato> list = campeonatoDAO.getAll(Campeonato.class);
		campeonatoDAO.closeEntityManager();
		return list;
	}
	public void alterar(Campeonato campeonato) {
		campeonatoDAO.save(campeonato);
		campeonatoDAO.closeEntityManager();
	}
	public void remover(Campeonato campeonato) {
		campeonato = campeonatoDAO.getById(Campeonato.class, campeonato.getIdCampeonato());
		campeonatoDAO.remove(campeonato);
		campeonatoDAO.closeEntityManager();
	}
	public Campeonato pesquisar(Campeonato campeonato)
	{
		campeonato = campeonatoDAO.getById(Campeonato.class, campeonato.getIdCampeonato());
		campeonatoDAO.closeEntityManager();
		return campeonato;
	}
	public List<Local> pesquisarLocaisCampeonato(Campeonato campeonato) 
	{
		List<Local> locais;
		campeonato = campeonatoDAO.getById(Campeonato.class, campeonato.getIdCampeonato());
		locais = campeonato.getLocais();
		return locais;
	}
	public List<Juiz> pesquisarJuizesCampeonato(Campeonato campeonato)
	{
		List<Juiz> juizes;
		campeonato = campeonatoDAO.getById(Campeonato.class, campeonato.getIdCampeonato());
		juizes = campeonato.getJuizes();
		return juizes;
	}
	public List<Categoria> pesquisarCategoriasCampeonato(Campeonato campeonato)
	{
		List <Categoria> categorias;
		campeonato = campeonatoDAO.getById(Campeonato.class, campeonato.getIdCampeonato());
		categorias = campeonato.getCategorias();
		return categorias;
	}
}