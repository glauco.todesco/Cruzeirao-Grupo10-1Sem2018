package sistema.service;

import java.util.List;

import sistema.dao.classes.EquipeDAO;
import sistema.modelos.Equipe;
import sistema.modelos.Usuario;

public class EquipeService
{
	EquipeDAO equipeDAO = new EquipeDAO();
	
	// M�todos
	public Equipe salvar(Equipe equipe) {
		equipe = equipeDAO.save(equipe);
		equipeDAO.closeEntityManager();
		return equipe;
	}
	public List<Equipe> getEquipes() {
		List<Equipe> list = equipeDAO.getAll(Equipe.class);
		equipeDAO.closeEntityManager();
		return list;
	}
	public void alterar(Equipe equipe) {
		equipeDAO.save(equipe);
		equipeDAO.closeEntityManager();
	}
	public void remover(Equipe equipe) {
		equipe = equipeDAO.getById(Equipe.class, equipe.getIdEquipe());
		equipeDAO.remove(equipe);
		equipeDAO.closeEntityManager();
	}
	public Equipe pesquisar(Equipe equipe)
	{
		equipe = equipeDAO.getById(Equipe.class, equipe.getIdEquipe());
		equipeDAO.closeEntityManager();
		return equipe;
	}
	public List<Usuario> pesquisarDiretoresEquipe(Equipe equipe)
	{
		List<Usuario> diretores;
		equipe = equipeDAO.getById(Equipe.class, equipe.getIdEquipe());
		diretores=equipe.getDiretores(); //Criar um m�todo que retorna s� os usuarios do tipo Diretor
		return diretores;
	}
}