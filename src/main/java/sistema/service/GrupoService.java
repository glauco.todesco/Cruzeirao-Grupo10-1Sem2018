package sistema.service;

import java.util.List;

import sistema.dao.classes.GrupoDAO;
import sistema.modelos.Grupo;
import sistema.modelos.Fase;
import sistema.modelos.Rodada;

public class GrupoService 
{
	GrupoDAO grupoDAO = new GrupoDAO();
	
	// M�todos
	public Grupo salvar(Grupo grupo) {
		grupo = grupoDAO.save(grupo);
		grupoDAO.closeEntityManager();
		return grupo;
	}
	public List<Grupo> getGrupos() {
		List<Grupo> list = grupoDAO.getAll(Grupo.class);
		grupoDAO.closeEntityManager();
		return list;
	}
	public void alterar(Grupo grupo) {
		grupoDAO.save(grupo);
		grupoDAO.closeEntityManager();
	}
	public void remover(Grupo grupo) {
		grupo = grupoDAO.getById(Grupo.class, grupo.getNumero());
		grupoDAO.remove(grupo);
		grupoDAO.closeEntityManager();
	}
	public Grupo pesquisar(Grupo grupo)
	{
		grupo = grupoDAO.getById(Grupo.class, grupo.getNumero());
		grupoDAO.closeEntityManager();
		return grupo;
	}
	public Fase pesquisarFaseGrupo(Grupo grupo)
	{
		Fase fase;
		grupo = grupoDAO.getById(Grupo.class, grupo.getNumero());
		fase = grupo.getFase();
		return fase;
	}
	public List<Rodada> pesquisarRodadasGrupo(Grupo grupo)
	{
		List<Rodada> rodadas;
		grupo = grupoDAO.getById(Grupo.class, grupo.getNumero());
		rodadas = grupo.getRodadas();
		return rodadas;
	}
}