package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Partida;

@FacesConverter("partidaConverter")
public class PartidaConverter implements Converter{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Partida par: Dados.partidas) {
				if(par.toString().equals(value))
					return par;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Partida) {
			return String.valueOf(((Partida) object).toString());
		}
		return null;
	}
}
