package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.dao.classes.InscricaoDAO;
import sistema.modelos.Inscricao;

@FacesConverter("inscricaoConverter")
public class InscricaoConverter implements Converter{
	private InscricaoDAO inscricaoDAO = new InscricaoDAO();
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Inscricao ins: inscricaoDAO.getAll(Inscricao.class)) {
				if(ins.toString().equals(value))
					return ins;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Inscricao) {
			return String.valueOf(((Inscricao) object).toString());
		}
		return null;
	}
}
