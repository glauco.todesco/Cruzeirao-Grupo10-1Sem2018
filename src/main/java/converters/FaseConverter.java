package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Fase;

@FacesConverter("faseConverter")
public class FaseConverter implements Converter{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Fase fas: Dados.fases) {
				if(fas.toString().equals(value))
					return fas;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Fase) {
			return String.valueOf(((Fase) object).toString());
		}
		return null;
	}
}
