package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Grupo;

@FacesConverter("grupoConverter")
public class GrupoConverter implements Converter{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Grupo gru: Dados.grupos) {
				if(gru.getNome().equals(value))
					return gru;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Grupo) {
			return String.valueOf(((Grupo) object).getNome());
		}
		return null;
	}
}
