package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.dao.classes.EquipeDAO;
import sistema.modelos.Equipe;

@FacesConverter("equipeConverter")
public class EquipeConverter implements Converter{
	
	private EquipeDAO equipeDAO = new EquipeDAO();
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Equipe equ: equipeDAO.getAll(Equipe.class)) {
				if(equ.getNome().equals(value))
					return equ;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Equipe) {
			return String.valueOf(((Equipe) object).getNome());
		}
		return null;
	}
}
