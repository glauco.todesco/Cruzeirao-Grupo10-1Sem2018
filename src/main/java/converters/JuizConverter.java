package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Juiz;

@FacesConverter("juizConverter")
public class JuizConverter implements Converter{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Juiz jui: Dados.juizes) {
				if(jui.toString().equals(value))
					return jui;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Juiz) {
			return String.valueOf(((Juiz) object).toString());
		}
		return null;
	}
}
