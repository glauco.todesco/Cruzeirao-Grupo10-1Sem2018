package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Categoria;

@FacesConverter("categoriaConverter")
public class CategoriaConverter implements Converter
{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
            for(Categoria cat: Dados.categorias) {
            	if(cat.getNome().equals(value))
            		return cat;
            }
        }       
         return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Categoria) {
            return String.valueOf(((Categoria) object).getNome());
        }
        else {
            return null;
        }
	}
}