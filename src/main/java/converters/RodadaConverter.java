package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Rodada;

@FacesConverter("rodadaConverter")
public class RodadaConverter implements Converter{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Rodada rod: Dados.rodadas) {
				if(rod.toString().equals(value))
					return rod;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Rodada) {
			return String.valueOf(((Rodada) object).toString());
		}
		return null;
	}
}
