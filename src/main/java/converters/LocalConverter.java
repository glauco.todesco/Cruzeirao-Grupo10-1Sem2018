package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.dao.classes.LocalDAO;
import sistema.modelos.Local;

@FacesConverter("localConverter")
public class LocalConverter implements Converter{
	private LocalDAO localDAO = new LocalDAO();
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Local loc: localDAO.getAll(Local.class)) {
				if(loc.toString().equals(value))
					return loc;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Local) {
			return String.valueOf(((Local) object).toString());
		}
		return null;
	}
}
