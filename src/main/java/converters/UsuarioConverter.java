package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.dao.classes.UsuarioDAO;
import sistema.modelos.Usuario;

@FacesConverter("usuarioConverter")
public class UsuarioConverter implements Converter{
	
	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Usuario u: usuarioDAO.getAll(Usuario.class)) {
				if(u.toString().equals(value))
					return u;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Usuario) {
			return String.valueOf(((Usuario) object).toString());
		}
		return null;
	}
}
