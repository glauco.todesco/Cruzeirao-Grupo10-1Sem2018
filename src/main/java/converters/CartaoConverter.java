package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Cartao;

@FacesConverter("cartaoConverter")
public class CartaoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Cartao car: Dados.cartaos) {
				if(car.toString().equals(value))
					return car;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Cartao) {
			return String.valueOf(((Cartao) object).toString());
		}
		return null;
	}
}
