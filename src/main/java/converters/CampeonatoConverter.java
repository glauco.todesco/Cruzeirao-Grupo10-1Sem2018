package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Campeonato;

@FacesConverter("campeonatoConverter")
public class CampeonatoConverter implements Converter
{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
            for(Campeonato cam: Dados.campeonatos) {
            	if(cam.getNome().equals(value))
            		return cam;
            }
        }        
         return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Campeonato) {
            return String.valueOf(((Campeonato) object).getNome());
        }
        else {
            return null;
        }
	}
}
