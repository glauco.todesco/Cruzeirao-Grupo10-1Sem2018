package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import dados.Dados;
import sistema.modelos.Inscrito;

@FacesConverter("inscritoConverter")
public class InscritoConverter implements Converter{
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if(value != null && value.trim().length() > 0) {
			for(Inscrito ins: Dados.inscritos) {
				if(ins.toString().equals(value))
					return ins;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if(object != null && object instanceof Inscrito) {
			return String.valueOf(((Inscrito) object).toString());
		}
		return null;
	}
}
