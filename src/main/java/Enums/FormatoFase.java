package Enums;

public enum FormatoFase 
{
	GRUPOS("Fase de Grupos"), 
	OITAVAS("Oitavas de Final"),
	QUARTAS("Quartas de Final"),
	SEMIFINAL("Semifinal"),
	TERCEIRO("Disputa de Terceiro Lugar"),
	FINAL("Final");
	
	private String descricao;

	private FormatoFase(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}