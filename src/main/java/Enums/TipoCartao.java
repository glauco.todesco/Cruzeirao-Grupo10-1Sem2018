package Enums;

public enum TipoCartao 
{
	AMARELO("Amarelo"),
	VERMELHO("Vermelho");
	
	private String descricao;

	private TipoCartao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
