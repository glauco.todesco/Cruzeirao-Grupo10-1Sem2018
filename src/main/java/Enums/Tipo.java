package Enums;

public enum Tipo 
{
	ADM("Administrador"),
	JUIZ("Juiz"),
	ORGANIZADOR("Organizador"),
	JOGADOR("Jogador"),
	TECNICO("T�cnico"),
	TORCEDOR("Torcedor"),
	DIRETOR("Diretor");
	
	private String descricao;

	private Tipo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}